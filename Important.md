This project is too large to view in Gitlab.

Please download the .ipynb file and use https://htmtopdf.herokuapp.com/ipynbviewer/ to view the full project.